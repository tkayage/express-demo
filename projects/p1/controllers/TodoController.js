var Todo = require('../models/Todo');
var ListTodo = require('package-demo');
module.exports = {

    create: function(req, res){
        var todo = new Todo({title:req.body.title});
        todo.save(function(err){
            if(!err){
                res.send("New record has been created");
            }else{
                console.log(err)
            }
        })
    },

    auto: function(req, res){
        for (let index = 0; index < 1000; index++) {
            var titled = req.body.title+" "+index;
            var todo = new Todo({title: titled});
            todo.save(function(err){
                res.status(500, {err});
            }); 
        }
        res.send("Done");
    },

    update: function(req, res){
        
    },

    get: function(req, res){

    },
    
    delete: function(req ,res){

    },

    index: function(req, res){
        ListTodo.run(Todo, function(data){
            if(data.err)    
                res.send(data.err);
            
            if(data.todos){
                res.send(data.todos)
            }
        });
    }
}